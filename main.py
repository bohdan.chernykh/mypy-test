import asyncio

from pysnail.pysnail import get_blazingly_fast_snail


async def main() -> None:
    print("Get ready!")
    snail = await get_blazingly_fast_snail()
    snail.replace("snail", "blabla")


if __name__ == "__main__":
    asyncio.run(main())

